# Build stage
FROM node:20.10.0 AS build

## Create app directory
WORKDIR /app

## Install app dependencies
COPY tsconfig*.json package*.json ./
RUN npm ci --omit=dev
RUN npm i -g @nestjs/cli@9.1.7

## Bundle app source
COPY ./config ./config
COPY ./src ./src

## Build app
RUN npm run build



# Run stage
FROM node:20.10.0

## Switch to less privileged user
USER node

## Declare env vars
ENV HEARTBEAT_SERVER_PORT=3000

ENV DB_CONNECTION=mysql
ENV DB_HOST=localhost
ENV DB_PORT=3306
ENV DB_USERNAME=heartbeat
ENV DB_PASSWORD=password
ENV DB_DATABASE=heartbeat
ENV DB_SYNCHRONIZE=false
ENV DB_LOGGING=false
ENV DB_ENTITIES=dist/**/*.entity.js

## Create app directory
WORKDIR /app

## Copy app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/config ./config
COPY --from=build /app/dist ./dist

## Execute app
CMD [ "node", "dist/main"]

## Expose port
EXPOSE 3000
