import { MigrationInterface, QueryRunner } from "typeorm";

export class AlterColumnNameEnabledInDevices1724602736764 implements MigrationInterface {
    name = 'AlterColumnNameEnabledInDevices1724602736764'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`devices\` CHANGE \`enabled\` \`alert_enabled\` tinyint NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`devices\` CHANGE \`alert_enabled\` \`enabled\` tinyint NOT NULL DEFAULT '0'`);
    }

}
