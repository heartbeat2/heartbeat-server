import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDevices1656489132353 implements MigrationInterface {
  name = 'CreateDevices1656489132353';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`devices\` (\`guid\` varchar(36) NOT NULL, \`description\` varchar(255) NOT NULL, \`max_timeout\` int NOT NULL DEFAULT '60', \`last_seen\` int NULL, \`type\` varchar(255) NULL, \`alert_sent_mail\` tinyint NOT NULL DEFAULT 0, \`alert_sent_discord\` tinyint NOT NULL DEFAULT 0, \`enabled\` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (\`guid\`)) ENGINE=InnoDB`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE \`devices\``);
  }
}
