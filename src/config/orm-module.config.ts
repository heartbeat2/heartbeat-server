import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { dbConfig } from './orm.config';

export const ormModuleConfig: TypeOrmModuleOptions = {
  type: (process.env.DB_TYPE ?? dbConfig.type) as any,
  host: process.env.DB_HOST ?? dbConfig.host,
  port: Number(process.env.DB_PORT ?? dbConfig.port),
  username: process.env.DB_USERNAME ?? dbConfig.user,
  password: process.env.DB_PASSWORD ?? dbConfig.pass,
  database: process.env.DB_DATABASE ?? dbConfig.database,
  dropSchema: dbConfig.dropSchema,
  synchronize: dbConfig.sync,
  migrationsRun: dbConfig.migrationsRun,
  autoLoadEntities: dbConfig.autoLoadEntities,
  ssl: JSON.parse(dbConfig.ssl),
  entities: [
    __dirname + '/../infrastructure/database/entities/*.entity{.ts,.js}',
  ],
  migrations: [__dirname + '/../infrastructure/database/migrations/*{.ts,.js}'],
};
