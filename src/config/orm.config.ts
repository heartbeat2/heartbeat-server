import { DatabaseType } from 'typeorm';
import * as config from 'config';

interface DatabaseConfig {
  type: DatabaseType;
  host: string;
  port: number;
  user: string;
  pass: string;
  database: string;
  dropSchema: boolean;
  sync: boolean;
  migrationsRun: boolean;
  autoLoadEntities: boolean;
  ssl: string;
}

export const dbConfig = config.get<DatabaseConfig>('db');
