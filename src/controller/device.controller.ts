import {
  Controller,
  Get,
  HttpCode,
  Inject,
  InternalServerErrorException,
} from '@nestjs/common';
import {
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { InjectMetric } from '@willsoto/nestjs-prometheus';
import { Counter, Histogram } from 'prom-client';
import { DeviceServiceInterface } from 'src/application/device.service.interface';
import { DevicesResponseDto } from './dtos/devices-response.dto';

@ApiTags('Device')
@Controller('devices')
export class DeviceController {
  private deviceService: DeviceServiceInterface;

  constructor(
    @Inject('DeviceService') deviceService: DeviceServiceInterface,
    @InjectMetric('http_requests_get_devices_success')
    public http_requests_get_devices_success: Counter<string>,
    @InjectMetric('http_requests_get_devices_failure')
    public http_requests_get_devices_failure: Counter<string>,
    @InjectMetric('http_requests_get_devices_duration')
    public http_requests_get_devices_duration: Histogram<string>,
  ) {
    this.deviceService = deviceService;
  }

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Returns devices.',
    description: 'Returns an array of all known devices.',
  })
  @ApiOkResponse({
    description: 'Devices were successfully returned.',
    type: DevicesResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error.',
  })
  async getDevices(): Promise<DevicesResponseDto> {
    const stopTimer = this.http_requests_get_devices_duration.startTimer();

    try {
      const devices = await this.deviceService.getAllDevices();

      const mappedDevices = devices.map((entry) => {
        return {
          description: entry.description,
          lastSeen: entry.lastSeen,
          maxTimeout: entry.maxTimeout,
          maxTimeoutExceeded: entry.maxTimeoutExceeded(),
          alertSentMail: entry.alertSentMail,
          alertSentDiscord: entry.alertSentDiscord,
        };
      });

      this.http_requests_get_devices_success.inc();
      return new DevicesResponseDto(mappedDevices);
    } catch (error) {
      console.error(error);
      this.http_requests_get_devices_failure.inc();

      throw new InternalServerErrorException();
    } finally {
      stopTimer();
    }
  }
}
