import {
  Body,
  Controller,
  HttpCode,
  Inject,
  InternalServerErrorException,
  NotFoundException,
  Post,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { InjectMetric } from '@willsoto/nestjs-prometheus';
import { Counter, Histogram } from 'prom-client';
import { PulseServiceInterface } from 'src/application/pulse.service.interface';
import { EntityNotFoundError } from 'typeorm';
import { PulseRequestDto } from './dtos/pulse-reqeust.dto';

@ApiTags('Pulse')
@Controller('pulse')
export class PulseController {
  private pulseService: PulseServiceInterface;

  constructor(
    @Inject('PulseService') pulseService: PulseServiceInterface,
    @InjectMetric('http_requests_beat_success')
    public http_requests_beat_success: Counter<string>,
    @InjectMetric('http_requests_beat_failure')
    public http_requests_beat_failure: Counter<string>,
    @InjectMetric('http_requests_beat_duration')
    public http_requests_beat_duration: Histogram<string>,
  ) {
    this.pulseService = pulseService;
  }

  @Post()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Updates the state of a device.',
    description:
      'Sets last_seen to the current timestamp, pulse_type to the received type and resets mail_sent to false.',
  })
  @ApiOkResponse({
    description: 'State of device was successfully updated.',
  })
  @ApiBadRequestResponse({
    description: 'Invalid value(s).',
  })
  @ApiNotFoundResponse({
    description: 'Device not found.',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error.',
  })
  async beat(@Body() pulseRequestDto: PulseRequestDto): Promise<void> {
    const stopTimer = this.http_requests_beat_duration.startTimer();

    try {
      await this.pulseService.beat(
        pulseRequestDto.deviceId,
        pulseRequestDto.type,
      );

      this.http_requests_beat_success.inc();
      return;
    } catch (error) {
      console.error(error);
      this.http_requests_beat_failure.inc();

      if (error instanceof EntityNotFoundError) {
        throw new NotFoundException();
      }
      throw new InternalServerErrorException();
    } finally {
      stopTimer();
    }
  }
}
