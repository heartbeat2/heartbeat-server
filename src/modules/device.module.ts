import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  makeCounterProvider,
  makeHistogramProvider,
} from '@willsoto/nestjs-prometheus';
import { DeviceService } from '../application/device.service';
import { DeviceController } from '../controller/device.controller';
import { DeviceEntity } from '../infrastructure/database/entities/device.entity';
import { DeviceRepository } from '../infrastructure/database/repositories/devices.repository';

@Module({
  imports: [TypeOrmModule.forFeature([DeviceEntity])],
  providers: [
    {
      provide: 'DeviceRepository',
      useClass: DeviceRepository,
    },
    {
      provide: 'DeviceService',
      useClass: DeviceService,
    },
    makeCounterProvider({
      name: 'http_requests_get_devices_success',
      help: 'Number of successful requests to getDevices() endpoint.',
    }),
    makeCounterProvider({
      name: 'http_requests_get_devices_failure',
      help: 'Number of failed requests to getDevices() endpoint.',
    }),
    makeHistogramProvider({
      name: 'http_requests_get_devices_duration',
      help: 'Measures the performance of requests to getDevices() endpoint.',
    }),
  ],
  controllers: [DeviceController],
})
export class DeviceModule {}
