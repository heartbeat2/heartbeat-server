import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ormModuleConfig } from 'src/config/orm-module.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PulseModule } from './pulse.module';
import { DeviceModule } from './device.module';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(ormModuleConfig),
    PrometheusModule.register(),
    DeviceModule,
    PulseModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
