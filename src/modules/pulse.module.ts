import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  makeCounterProvider,
  makeHistogramProvider,
} from '@willsoto/nestjs-prometheus';
import { PulseService } from '../application/pulse.service';
import { PulseController } from '../controller/pulse.controller';
import { DeviceEntity } from '../infrastructure/database/entities/device.entity';
import { DeviceRepository } from '../infrastructure/database/repositories/devices.repository';

@Module({
  imports: [TypeOrmModule.forFeature([DeviceEntity])],
  providers: [
    {
      provide: 'DeviceRepository',
      useClass: DeviceRepository,
    },
    {
      provide: 'PulseService',
      useClass: PulseService,
    },
    makeCounterProvider({
      name: 'http_requests_beat_success',
      help: 'Number of successful requests to beat() endpoint.',
    }),
    makeCounterProvider({
      name: 'http_requests_beat_failure',
      help: 'Number of failed requests to beat() endpoint.',
    }),
    makeHistogramProvider({
      name: 'http_requests_beat_duration',
      help: 'Measures the performance of requests to beat() endpoint.',
    }),
  ],
  controllers: [PulseController],
})
export class PulseModule {}
